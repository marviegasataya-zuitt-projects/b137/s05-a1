package b137.gasataya.s05a1;

import java.util.ArrayList;

public class Phonebook {
    // Properties
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // Empty constructor
    public Phonebook() {
    }

    // Parameterized constructor
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    // Getters
    public ArrayList<Contact> getPhonebook() {
        return contacts;
    }

    // Setters
    public void setPhonebook(ArrayList<Contact> phoneBook) {
        this.contacts = phoneBook;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    // Other
    public boolean isEmpty() {
        return this.contacts.size() == 0;
    }
}
