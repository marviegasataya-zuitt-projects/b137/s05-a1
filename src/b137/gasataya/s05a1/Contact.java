package b137.gasataya.s05a1;

import java.util.ArrayList;

public class Contact {
    // Properties
    private String name;
    private ArrayList<String> numbers = new ArrayList<String>();
    private ArrayList<String> addresses = new ArrayList<String>();

    // Empty constructor
    public Contact() {
    }

    // Parameterized constructor
    public Contact(String name, ArrayList<String> numbers, ArrayList<String> addresses) {
        this.name = name;
        this.numbers = numbers;
        this.addresses = addresses;
    }

    // Getters
    public String getName() {
        return name;
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

    public ArrayList<String> getAddresses() {
        return addresses;
    }

    // Setters
    public void setName(String newName) {
        this.name = newName;
    }

    public void setNumbers(ArrayList<String> numbers) {
        this.numbers = numbers;
    }

    public void addNumber(String number) {
        this.numbers.add(number);
    }

    public void setAddresses(ArrayList<String> addresses) {
        this.addresses = addresses;
    }

    public void addAddress(String address) {
        this.addresses.add(address);
    }

    // Printers
    public void printNumbers() {
        for (String number : this.numbers) {
            System.out.println(number);
        }
    }

    public void printAddresses() {
        for (String name : this.addresses) {
            System.out.println(name);
        }
    }
}
